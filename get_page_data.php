<?php

    /**
     * Ejemplo para obtener el tamano de una pagina
     */

    include_once 'firmamex_services.php';


    $webId = '';
    $apiKey = '';

    $firmamexServices = new FirmamexServices($webId, $apiKey);
    
    $file = file_get_contents('file.pdf');
    
    $data = json_decode(
        $firmamexServices -> getPageData($file, 10)
    );

    echo json_encode($data);

?>