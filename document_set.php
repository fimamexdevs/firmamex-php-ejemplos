<?php

    /**
     * Ejemplo de expedientes
     */

    include_once("../php/firmamex_services.php");

	$webId = "";
    $apiKey = "";

    $firmamexServices = new FirmamexServices($webId, $apiKey);
    
    $documentSetResponse = json_decode($firmamexServices -> createDocumentSet("set ejemplo"));
    $documentSetId = $documentSetResponse -> document_set;
    
    $firmamexServices -> request((object)[
        "document_set" => $documentSetId,
        "url_doc" => "https://www.dropbox.com/s/sxvgq1uhb4k3s4w/contrato.pdf?dl=0",
        "stickers" => [
                    (object)[
                        "authority" => "SAT",
                        "stickerType" => "line",
                        "dataType" => "rfc",
                        "imageType" => "desc",
                        "data" => "GOCF9002226A11",
                        "email" => "fernando@firmamex.com",
                        "page" => 0,
                        "rect" => (object)[
                            "lx" => 330,
                            "ly" => 300,
                            "tx" => 530,
                            "ty" =>  400
                        ]
                    ]                 , 
                 (object)[
                    "authority" => "Vinculada a Correo Electronico por Liga",
                    "stickerType" => "line",
                    "dataType" => "email",
                    "imageType" => "desc",
                    "data" => "fernando@firmamex.com",
                    "page" => 0,
                    "rect" => (object)[
                        "lx" => 130,
                        "ly" => 200,
                        "tx" => 430,
                        "ty" =>  300
                    ]
                 ]
        ]
    ]);
    
    $closeParams = new StdClass();
    $closeParams -> documentSet = $documentSetId;
    $closeParams -> workflow = (object)[
        "remind_every" => "1d",
        "language" => "es",
        "ordered" => [
            "GOCF9002226A11",
            "fernando@firmamex.com"
        ]
    ];

    
    $firmamexServices -> closeDocumentSet($closeParams);

    $documentSetData = $firmamexServices -> getDocumentSet($documentSetId);
    echo json_encode(json_decode($documentSetData), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

?>