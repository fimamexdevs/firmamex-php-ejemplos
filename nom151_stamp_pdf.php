<?php

    /**
     * Ejemplo para obtener una estampilla de tiempo
     */

    include_once('firmamex_services.php');

    $webId = '';
    $apiKey = '';

    $firmamexServices = new FirmamexServices($webId, $apiKey);
    
    // lee el archivo
    $file = file_get_contents('doc.pdf');

    // le incrusta sello de tiempo
    $stamped = json_decode($firmamexServices -> nom151Stamp($file));

    // guarda el archivo con sello de tiempo
    file_put_contents('stamped.pdf', base64_decode($stamped -> document));
    // JSON con informacion de validacion de sello de tiempo
    file_put_contents('timestampData.json', json_encode($stamped -> timestampData));

?>