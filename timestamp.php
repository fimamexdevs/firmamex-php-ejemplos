<?php

    /**
     * Ejemplo para obtener una estampilla de tiempo
     */

    include_once('firmamex_services.php');

    $webId = '';
    $apiKey = '';

    $firmamexServices = new FirmamexServices($webId, $apiKey);
    
    $hexHash = hash('sha256', 'datos a estampillar', FALSE);
    
    // genera sello de tiempo
    $timestamp = json_decode($firmamexServices->timestamp('{"hash":"'.$hexHash.'"}'));

    // valida sello de tiempo
    echo $firmamexServices -> timestampValidateHash((object)[
        hash => $hexHash,
        timestamp => $timestamp -> timestamp
    ])

?>