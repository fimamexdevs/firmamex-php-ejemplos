
<?php

    /**
     * Ejemplo de inyectar datos en un template HTML 
     * con dos stickers definidos por medio de recuadros
     */

    include_once('../php/firmamex_services.php');

    $webId = '';
    $apiKey = '';

    $firmamexServices = new FirmamexServices($webId, $apiKey);

    $options = new \stdClass();
    $options -> template_flatten = true;
    $options -> template_title = 'ejemplo1';
    $options -> fields = [
        (object)[
            'value' => '12310',
            'id' => 'codigo'
        ],
        (object)[
            'value' => 'ÁÉÍ',
            'id' => 'proveedor'
        ],
        (object)[
            'value' => '',
            'id' => 'empresa'
        ],
        (object)[
            'value' => 'Firmamex GOCF9002226A7 line dibujo',
            'id' => 'sign_1'
        ],
        (object)[
            'value' => 'Firmamex fernando@firmamex.com',
            'id' => 'sign_2'
        ]
    ];
    $options -> tables = [
        (object)[
            'id' => 'directory',
            'rows' => [
                ['clave', 'desc', 'cant', 'precio', 'total'],
                ['clave 2', 'desc 2', 'cant 2', 'precio 2', 'total 2']
            ]
        ]
    ];

    $response = $firmamexServices->request(json_encode($options));
    echo json_encode(json_decode($response), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
 ?>