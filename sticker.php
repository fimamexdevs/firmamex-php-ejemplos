<?php

    /**
     * Ejemplo para generar un documento a partir de una URL colocando
     * los stickers por medio del API
     */

    include_once('../php/firmamex_services.php');

    $webId = '';
    $apiKey = '';

   $firmamexServices = new FirmamexServices($webId, $apiKey);

   $options = (object)[
       'url_doc' => 'https://www.dropbox.com/s/sxvgq1uhb4k3s4w/contrato.pdf?dl=0',
       'stickers' => [
           (object)[
                "authority" => "SAT",
                "stickerType" => "line",
                "dataType" => "rfc",
                "imageType" => "desc",
                "data" => "GOCF9002226A7",
                "page" => 0,
                "rect" => (object)[
                    "lx" => 355,
                    "ly" => 102,
                    "tx" => 555,
                    "ty" =>  202
                ]
           ]
       ]
   ];

   $response = $firmamexServices->request($options);
    echo json_encode(json_decode($response), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

?>