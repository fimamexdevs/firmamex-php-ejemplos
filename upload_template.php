<?php

    /**
     * Ejemplo de dar de alta un template HTML
     */

    include_once('../php/firmamex_services.php');

    $webId = '';
    $apiKey = '';

    $firmamexServices = new FirmamexServices($webId, $apiKey);

    $templateData = '
    <html>
    <style>
    thead,

        table {
            border: 1px solid black;
            border-collapse: collapse;
            letter-spacing: 1px;
        }

        th {        
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }

        td {
            padding: 10px;
            border: 1px solid black;
            text-align: center;
        }

        td, th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        table tr:nth-child(even){background-color: #f2f2f2;}
    </style>
    <body>
        <div style="margin-left: 50px;">
            <p style="text-align:justify;">
                <div style="">
                    <span>Codigo: </span>
                    <span class="signmage-template-field" id="codigo"></span>
                </div>
            </p>
            <p style="text-align:justify;">
                <div style="">
                    <span>Proveedor: </span>
                    <span class="signmage-template-field" id="proveedor"></span>
                </div>
            </p>
            <p style="text-align:justify;">
                <span>Empresa: </span>
                <span class="signmage-template-field" id="empresa"></span>
            </p>
            <table class="signmage-template-table" id="directory" style="width: 100%;">
                <thead style="style="border-bottom: 1px solid black">
                    <tr style="height: 31px;">
                        <th>
                            <strong>Clave</strong>
                        </th>
                        <th>
                            <strong>Descripción</strong>
                        </th>
                        <th>
                            <strong>Cantidad</strong>
                        </th>
                        <th>
                            <strong>Precio</strong>
                        </th>
                        <th>
                            <strong>Total</strong>
                        </th>
                    </tr>
                </thead>

                <tbody>
                </tbody>

            </table>

            <br/>            
        </div>

        <div style="border:1px solid black; float:left; width:200px; height:120px; margin:auto; color:white; word-break:break-all; font-size:2px;">
            <span class="signmage-template-field" id="sign_1"></span>
        </div>

        <div style="border:1px solid black; float:right; width:200px; height:120px; margin:auto; color:white; word-break:break-all; font-size:2px;">
            <span class="signmage-template-field" id="sign_2"></span>
        </div>

    </body>

    </html>
    ';

    $templateData = base64_encode($templateData);

    $options = (object)[
        'template_type' => 'html',
        'template_data' => $templateData,
        'template_name' => 'ejemplo1'
    ];

    $response = $firmamexServices->saveTemplate(json_encode($options));
    echo json_encode(json_decode($response), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
	
 ?>